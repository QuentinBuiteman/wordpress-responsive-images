<?php
/**
 * Generation of image sizes on WP upload
 *
 * PHP version 7
 *
 * @package WordPressResponsive\Images
 * @author  SchaapOntwerpers <info@schaapontwerpers.nl>
 * @license https://opensource.org/licenses/MIT MIT
 */

namespace WordPressResponsive;

use WordPressPluginAPI\FilterHook;
use EasyDOM\DOMDocument;

class Images implements FilterHook
{
    /**
     * Allowed file types to use for generating more images
     *
     * @var array
     */
    const EXTENSIONS = ['png', 'jpg', 'jpeg', 'bmp', 'gif'];

    /**
     * Maximum resolution of images uploads
     *
     * @var int
     */
    const IMAGE_SIZES = ['medium', 'medium_large', 'large', 'original'];

    /**
     * Maximum resolution of images uploads
     *
     * @var int
     */
    const MAX_SIZE = 2560;

    /**
     * Subscribe functions to corresponding filters
     *
     * @return array
     */
    public static function getFilters(): array
    {
        return array(
            'big_image_size_threshold' => 'disableTreshold',
            'image_size_names_choose' => 'srcsetSizes',
            'intermediate_image_sizes_advanced' => array('imgSizes', 10, 2),
            'wp_update_attachment_metadata' => 'resetSizes',
        );
    }

    /**
     * Disable the big treshold, this is already handled by optimizing the image
     *
     * @return bool
     */
    public function disableTreshold(): bool
    {
        return false;
    }

    /**
     * Change default image sizes to fit uploaded image
     *
     * @param array $sizes Sizes to generate for uploaded images
     * @param array $meta  Metadata from the uploaded image
     *
     * @return array
     */
    public function imgSizes(array $sizes, array $meta): array
    {
        // Unset unneeded sizes
        unset($sizes['2048x2048']);
        unset($sizes['1536x1536']);

        // Optimize original if it's bigger than allowed max size
        if (($meta['width'] > self::MAX_SIZE) ||
            ($meta['height'] > self::MAX_SIZE)) {
            $meta = $this->resizeOriginal($meta);
        }

        return $this->calculateSizes(
            $meta['width'],
            $meta['height'],
            $sizes,
            false,
        );
    }

    /**
     * Reset sizes to max to prevent sizes conflicts
     *
     * @param array $meta Meta information of uploaded image
     *
     * @return array
     */
    public function resetSizes(array $meta): array
    {
        foreach (self::IMAGE_SIZES as $size) {
            if ($size == 'original') {
                continue;
            }

            update_option($size . '_size_w', 9999);
            update_option($size . '_size_h', 9999);
        }

        return $meta;
    }

    /**
     * Change default returned sizes for srcset
     *
     * @param array $sizes Sizes that it automatically returns
     *
     * @return array
     */
    public function srcsetSizes(array $sizes): array
    {
        return array_merge($sizes, [
            'medium_large' => 'medium_large',
        ]);
    }

    /**
     * Resize original image and return new meta data
     *
     * @param array $meta Meta information of uploaded image
     *
     * @return array
     */
    private function resizeOriginal(array $meta): array
    {
        // Width/height
        $width = $meta['width'];
        $height = $meta['height'];

        // Location
        $uploadDir = wp_upload_dir();
        $uploadPath = $uploadDir['basedir'];
        $imgPath = $uploadPath . '/' . $meta['file'];

        // Calculate new width/height
        if ($width == $height) {
            $newWidth = self::MAX_SIZE;
            $newHeight = self::MAX_SIZE;
        } else {
            $isLandscape = $width > $height;
            $newWidth = $isLandscape ?
                self::MAX_SIZE :
                ($width / $height) * self::MAX_SIZE;
            $newHeight = $isLandscape ?
                ($height / $width) * self::MAX_SIZE :
                self::MAX_SIZE;
        }

        // Resize image
        $imagick = new \Imagick($imgPath);
        $imagick->resizeImage(
            $newWidth,
            $newHeight,
            \Imagick::FILTER_UNDEFINED,
            1,
        );
        $imagick->writeImage($imgPath);
        $imagick->clear();

        // Set new metadata
        $meta['width'] = $newWidth;
        $meta['height'] = $newHeight;

        return $meta;
    }

    /**
     * Calculate sizes based on original image size
     *
     * @param int   $width    Width of the original image
     * @param int   $height   Height of the original image
     * @param array $original Optional original sizes array to edit
     * @param bool  $update   If the option data must be updated
     *
     * @return array
     */
    private function calculateSizes(
        int $width,
        int $height,
        array $original = [],
        bool $update = true
    ): array {
        // Sizes to create, 4 is original
        $multipliers = array(
            'medium' => 1,
            'medium_large' => 2,
            'large' => 3,
        );

        // Width and height to calculate with
        $calcWidth = (int)round($width / 4);
        $calcHeight = (int)round($height / 4);

        foreach ($multipliers as $size => $multiplier) {
            $newWidth = $calcWidth * $multiplier;
            $newHeight = $calcHeight * $multiplier;

            if ($update) {
                update_option($size . '_size_w', $newWidth);
            }

            $original[$size] = [
                'width' => $newWidth,
                'height' => $newHeight,
                'crop' => false,
            ];
        }

        return $original;
    }
}
